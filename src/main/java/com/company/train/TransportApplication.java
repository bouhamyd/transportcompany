package com.company.train;

import com.company.train.models.output.CustomerSummaries;
import com.company.train.models.input.Taps;
import com.company.train.services.CutomerSummarieService;
import com.company.train.services.TapService;
import com.company.train.util.TransportException;

public class TransportApplication {

    public static void main(String[] args) {
        if (args == null ? true : (args.length == 0 | args.length < 2 | args.length > 2) ? true : false) {
            System.out.println("Error : Need two files");
            return;
        }

        if (args == null ? true : args.length == 2 ? true : false) {
            TransportApplication transportApplication = new TransportApplication();
            try {
                transportApplication.getCustomerTrips(args[0], args[1]);
            } catch (TransportException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
            return;
        }
    }

    void getCustomerTrips(String inputFilePath, String outputFilePath) throws TransportException {
        TapService tapService = new TapService();
        Taps taps = tapService.getTapsFromFile(inputFilePath);
        CutomerSummarieService cutomerSummarieService = new CutomerSummarieService();
        CustomerSummaries customerSummaries = cutomerSummarieService.getCustomerSummaries(taps.getTaps());
        cutomerSummarieService.customerSummariesToJson(customerSummaries, outputFilePath);
    }
}
