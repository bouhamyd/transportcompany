package com.company.train.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.DecimalFormat;

public class DecimalJsonSerializer extends JsonSerializer<Double> {

    @Override
     public void serialize(Double value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
             DecimalFormat format = new DecimalFormat("0.#");
             format.format(value);
             gen.writeNumber(format.format(value));
    }
}
