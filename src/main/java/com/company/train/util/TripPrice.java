package com.company.train.util;

import java.util.Arrays;

public enum TripPrice {

    AA("1", "1", 2.40),
    AB("1", "2", 2.40),
    AC("1", "3", 2.80),
    AD("1", "4", 3.00),

    BA("2", "1", 2.40),
    BB("2", "2", 2.40),
    BC("2", "3", 2.80),
    BD("2", "4", 3.00),

    CA("3", "1", 2.80),
    CB("3", "2", 2.80),
    CC("3", "3", 2.00),
    CD("3", "4", 2.00),

    DA("4", "1", 3.00),
    DB("4", "2", 3.00),
    DC("4", "3", 2.00),
    DD("4", "4", 2.00),
    NOTFOUND("x", "x", 0.00);

    private String departure;
    private String destination;
    private double price;

    TripPrice() {
    }

    TripPrice(String departure, String destination, double price) {
        this.departure = departure;
        this.destination = destination;
        this.price = price;
    }

    public String getDeparture() {
        return departure;
    }

    public String getDestination() {
        return destination;
    }

    public double getPrice() {
        return price;
    }

    public static TripPrice findByDepartureAndDestination(String departure, String destination) throws TransportException {
        TripPrice trip = Arrays.stream(TripPrice.values()).filter(tripPrice -> tripPrice.departure.equals(departure) && tripPrice.destination.equals(destination)).findFirst().orElse(TripPrice.NOTFOUND);
        if (trip != TripPrice.NOTFOUND) {
            return trip;
        } else {
            throw new TransportException(500,"");
        }
    }

}
