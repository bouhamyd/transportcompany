package com.company.train.services;

import com.company.train.models.Zone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ZoneService {

    List<Zone> zones = new ArrayList<Zone>();

    public ZoneService() {
        initZonnes();
    }

    public void initZonnes() {
        addZone(new Zone(1, Arrays.asList("A", "B")));
        addZone(new Zone(2, Arrays.asList("C", "D", "E")));
        addZone(new Zone(3, Arrays.asList("C", "E", "F")));
        addZone(new Zone(4, Arrays.asList("F", "G", "H", "I")));
    }

    List<Zone> addZone(Zone zone) {
        zones.add(zone);
        return zones;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public List<Zone> getZoneByStation(String station) {
        return zones.stream().filter(zone -> {
            if (zone.getStations().contains(station)) {
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());
    }
}

