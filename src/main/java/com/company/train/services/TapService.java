package com.company.train.services;

import com.company.train.models.input.Taps;
import com.company.train.util.TransportException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class TapService {

    public Taps getTapsFromFile(String inputFile) throws TransportException {
        ObjectMapper mapper = new ObjectMapper();
        Taps taps = new Taps();
        try {
            taps = mapper.readValue(new File(inputFile), Taps.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new TransportException(500,"Sorry, We are meet technical problem");
        } catch (IOException e) {
            e.printStackTrace();
            throw new TransportException(500,"File not found");
        }
        return taps;
    }
}
