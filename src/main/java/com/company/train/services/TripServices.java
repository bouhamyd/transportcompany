package com.company.train.services;

import com.company.train.models.*;
import com.company.train.models.input.Tap;
import com.company.train.models.output.CustomerSummarie;
import com.company.train.models.output.CustomerSummaries;
import com.company.train.models.output.Trip;
import com.company.train.util.TransportException;
import com.company.train.util.TripPrice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TripServices {

    ZoneService zoneService = new ZoneService();

    public List<Trip> getTrips(List<Tap> taps) {
        List<Trip> trips = new ArrayList<>();
        for (int i = 0; i < taps.size(); i++) {
            Tap departure = taps.get(i);
            Tap destination = null;
            if (taps.size() > i + 1) {
                destination = taps.get(++i);
            }
            Trip trip = getTrip(departure.getStation(), destination.getStation());
            trip.setStartedJourneyAt(departure.getUnixTimestamp());
            trips.add(trip);
        }
        return trips;
    }

    public Trip getTrip(String departureStation, String destinationStation) {
        Trip trip = new Trip();
        trip.setCostInCents(10000000);
        List<Zone> zonesDeparture = zoneService.getZoneByStation(departureStation);
        List<Zone> zonesDestination = zoneService.getZoneByStation(destinationStation);
        zonesDeparture.forEach(zoneDeparture -> {
            zonesDestination.forEach(zoneDestination -> {
                double tripPrice = 0;
                try {
                    tripPrice = calculateTripPrice(String.valueOf(zoneDeparture.getName()), String.valueOf(zoneDestination.getName()));
                } catch (TransportException e) {
                    e.printStackTrace();
                }
                if (tripPrice < trip.getCostInCents()) {
                    trip.setZoneFrom(zoneDeparture.getName());
                    trip.setZoneTo(zoneDestination.getName());
                    trip.setStationStart(departureStation);
                    trip.setStationEnd(destinationStation);
                    trip.setCostInCents(tripPrice*100);
                }
            });
        });
        return trip;
    }

    public double calculateTripPrice(String zoneDeparture, String zoneDestination) throws TransportException {
        TripPrice tripPrice = TripPrice.findByDepartureAndDestination(zoneDeparture, zoneDestination);
        return tripPrice.getPrice();
    }

}
