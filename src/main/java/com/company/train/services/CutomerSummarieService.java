package com.company.train.services;

import com.company.train.models.input.Tap;
import com.company.train.models.output.CustomerSummarie;
import com.company.train.models.output.CustomerSummaries;
import com.company.train.models.output.Trip;
import com.company.train.util.TransportException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CutomerSummarieService {

    TripServices tripServices = new TripServices();


    public CustomerSummarie getCustomerSummaire(List<Tap> taps) {
        CustomerSummarie customerSummarie = new CustomerSummarie();
        List<Trip> trips = tripServices.getTrips(taps);
        customerSummarie.setTrips(trips);
        customerSummarie.setCustomerId(taps.get(0).getCustomerId());
        double totalCostInCent = trips.stream().map(x -> x.getCostInCents()).collect(Collectors.summingInt(Double::intValue));
        customerSummarie.setTotalCostInCents(totalCostInCent);
        return customerSummarie;
    }

    public CustomerSummaries getCustomerSummaries(List<Tap> taps) {
        CustomerSummaries customerSummaries = new CustomerSummaries();
        customerSummaries.setCustomerSummaries(Arrays.asList(getCustomerSummaire(taps)));
        return customerSummaries;
    }

    public void customerSummariesToJson(CustomerSummaries customerSummaries, String outputFile) throws TransportException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(outputFile), customerSummaries);
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(customerSummaries);
        }catch (IOException e) {
            e.printStackTrace();
            throw new TransportException(500,"File not found");
        }
    }
}
