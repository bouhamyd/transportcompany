package com.company.train.models.input;

import com.company.train.models.input.Tap;

import java.util.ArrayList;
import java.util.List;

public class Taps {

    List<Tap> taps = new ArrayList<Tap> ();

    public Taps(){
    }

    public Taps(List<Tap> taps) {
        this.taps = taps;
    }

    public List<Tap> getTaps() {
        return taps;
    }

    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }
}
