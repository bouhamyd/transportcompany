package com.company.train.models.output;

import com.company.train.config.DecimalJsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Trip {
    private String stationStart;
    private String stationEnd;
    private long startedJourneyAt;
    @JsonSerialize(using= DecimalJsonSerializer.class)
    private double costInCents;
    private int zoneFrom;
    private int zoneTo;

    public Trip(){
    }

    public String getStationStart() {
        return stationStart;
    }

    public void setStationStart(String stationStart) {
        this.stationStart = stationStart;
    }

    public String getStationEnd() {
        return stationEnd;
    }

    public void setStationEnd(String stationEnd) {
        this.stationEnd = stationEnd;
    }

    public long getStartedJourneyAt() {
        return startedJourneyAt;
    }

    public void setStartedJourneyAt(long startedJourneyAt) {
        this.startedJourneyAt = startedJourneyAt;
    }

    public double getCostInCents() {
        return costInCents;
    }

    public void setCostInCents(double costInCents) {
        this.costInCents = costInCents;
    }

    public int getZoneFrom() {
        return zoneFrom;
    }

    public void setZoneFrom(int zoneFrom) {
        this.zoneFrom = zoneFrom;
    }

    public int getZoneTo() {
        return zoneTo;
    }

    public void setZoneTo(int zoneTo) {
        this.zoneTo = zoneTo;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "stationStart='" + stationStart + '\'' +
                ", stationEnd='" + stationEnd + '\'' +
                ", startedJourneyAt='" + startedJourneyAt + '\'' +
                ", costInCents=" + costInCents +
                ", zoneFrom='" + zoneFrom + '\'' +
                ", zoneTo='" + zoneTo + '\'' +
                '}';
    }
}
