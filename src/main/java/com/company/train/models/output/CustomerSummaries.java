package com.company.train.models.output;

import com.company.train.models.output.CustomerSummarie;

import java.util.ArrayList;
import java.util.List;

public class CustomerSummaries {

    private List<CustomerSummarie> customerSummaries = new ArrayList<>();

    public CustomerSummaries(){

    }

    public CustomerSummaries(List<CustomerSummarie> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }

    public List<CustomerSummarie> getCustomerSummaries() {
        return customerSummaries;
    }

    public void setCustomerSummaries(List<CustomerSummarie> customerSummaries) {
        this.customerSummaries = customerSummaries;
    }

    @Override
    public String toString() {
        return "CustomerSummaries{" +
                "customerSummaries=" + customerSummaries.toString()+
                '}';
    }
}
