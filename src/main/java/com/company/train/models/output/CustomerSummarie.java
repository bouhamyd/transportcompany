package com.company.train.models.output;

import com.company.train.config.DecimalJsonSerializer;
import com.company.train.models.output.Trip;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

public class CustomerSummarie {

	  	private int customerId;
        @JsonSerialize(using = DecimalJsonSerializer.class)
        private double totalCostInCents;
        private List<Trip> trips = new ArrayList<>();

    public CustomerSummarie(){
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public double getTotalCostInCents() {
        return totalCostInCents;
    }

    public void setTotalCostInCents(double totalCostInCents) {
        this.totalCostInCents = totalCostInCents;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    @Override
    public String toString() {
        return "CustomerSummarie{" +
                "customerId=" + customerId +
                ", totalCostInCents=" + totalCostInCents +
                ", trips=" + trips +
                '}';
    }
}
