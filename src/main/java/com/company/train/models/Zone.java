package com.company.train.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Zone {
    private int name;
    private List<String> stations = new ArrayList<String>();

    public Zone(){
    }

    public Zone(int name, List<String> stations) {
        this.name = name;
        this.stations = stations;
    }

    public List<String> getStations() {
        return stations;
    }

    public void setStatus(List<String> stations) {
        this.stations = stations;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zone zone = (Zone) o;
        return name == zone.name &&
                stations.equals(zone.stations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, stations);
    }
}
