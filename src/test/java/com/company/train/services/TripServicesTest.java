package com.company.train.services;

import com.company.train.models.input.Tap;
import com.company.train.models.output.CustomerSummarie;
import com.company.train.models.output.CustomerSummaries;
import com.company.train.models.output.Trip;
import com.company.train.util.TransportException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TripServicesTest {

    TripServices tripServices = new TripServices();

    @Test
    public void getTrips() {
        List<Tap> taps = new ArrayList<Tap>();
        taps.add(new Tap(1572242400,1,"A"));
        taps.add(new Tap(1572244200,1,"D"));
        List<Trip> actualTrips = tripServices.getTrips(taps);
        Trip expectedTrip= new Trip();
        expectedTrip.setZoneFrom(1);
        expectedTrip.setZoneTo(2);
        expectedTrip.setStationStart("A");
        expectedTrip.setStationEnd("D");
        expectedTrip.setCostInCents(240);
        expectedTrip.setStartedJourneyAt(1572242400);
        assertEquals(actualTrips.get(0).getStationStart(),expectedTrip.getStationStart());
        assertEquals(actualTrips.get(0).getStationEnd(),expectedTrip.getStationEnd());
        assertEquals(String.valueOf(actualTrips.get(0).getCostInCents()),String.valueOf(expectedTrip.getCostInCents()));
        assertEquals(String.valueOf(actualTrips.get(0).getZoneFrom()),String.valueOf(expectedTrip.getZoneFrom()));
        assertEquals(String.valueOf(actualTrips.get(0).getZoneTo()),String.valueOf(expectedTrip.getZoneTo()));
        assertEquals(String.valueOf(actualTrips.get(0).getStartedJourneyAt()),String.valueOf(expectedTrip.getStartedJourneyAt()));
    }

    @Test
    public void getTrip() {
        Trip actualTrip = tripServices.getTrip("A","D");
        Trip expectedTrip= new Trip();
        expectedTrip.setZoneFrom(1);
        expectedTrip.setZoneTo(2);
        expectedTrip.setStationStart("A");
        expectedTrip.setStationEnd("D");
        expectedTrip.setCostInCents(240);
        assertEquals(actualTrip.getStationStart(),expectedTrip.getStationStart());
        assertEquals(actualTrip.getStationEnd(),expectedTrip.getStationEnd());
        assertEquals(String.valueOf(actualTrip.getCostInCents()),String.valueOf(expectedTrip.getCostInCents()));
        assertEquals(String.valueOf(actualTrip.getZoneFrom()),String.valueOf(expectedTrip.getZoneFrom()));
        assertEquals(String.valueOf(actualTrip.getZoneTo()),String.valueOf(expectedTrip.getZoneTo()));
    }

    @Test
    public void calculateTripPrice() throws TransportException {
        double actual = tripServices.calculateTripPrice("1","2");
        double expected = 2.40;
        assertEquals(String.valueOf(actual),String.valueOf(expected));
    }

    @Test(expected = TransportException.class)
    public void calculateTripPriceError() throws TransportException {
        double actual = tripServices.calculateTripPrice("1","100");
        double expected = 2.40;
        assertEquals(String.valueOf(actual),String.valueOf(expected));
    }


}