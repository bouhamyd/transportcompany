package com.company.train.services;

import com.company.train.models.input.Taps;
import com.company.train.util.TransportException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class TapServiceTest {

    TapService tapService = new TapService();

    @Test
    public void getTapsFromFile_Should_Return_Exception() {
        Taps taps = new Taps();
        try {
            taps = tapService.getTapsFromFile("notfound.txt");
            fail("should throw TransportException");
        }catch (TransportException e){
            assert(e.getMessage().contains("File not found"));
        }
    }

    @Test
    public void getTapsFromFile() throws IOException, TransportException {
        Taps actualsTaps = new Taps();
        actualsTaps = tapService.getTapsFromFile("CandidateInputExample.txt");
        ObjectMapper mapper = new ObjectMapper();
        Taps expectedTaps = mapper.readValue("{ \"taps\": [ { \"unixTimestamp\": 1572242400, \"customerId\": 1, \"station\": \"A\" }, { \"unixTimestamp\": 1572244200, \"customerId\": 1, \"station\": \"D\" }, { \"unixTimestamp\": 1572282000, \"customerId\": 1, \"station\": \"D\" }, { \"unixTimestamp\": 1572283800, \"customerId\": 1, \"station\": \"A\" } ] }", Taps.class);
        assertEquals(actualsTaps.getTaps().size(),expectedTaps.getTaps().size());
        assertEquals(actualsTaps.getTaps().get(0).getUnixTimestamp(),expectedTaps.getTaps().get(0).getUnixTimestamp(),0);
        assertEquals(actualsTaps.getTaps().get(0).getCustomerId(),expectedTaps.getTaps().get(0).getCustomerId(),0);
        assertEquals(actualsTaps.getTaps().get(0).getStation(),expectedTaps.getTaps().get(0).getStation());
    }

}