package com.company.train.services;

import com.company.train.models.Zone;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ZoneServiceTest {

    ZoneService zoneService = new ZoneService();
    @Test
    public void getZones() {
        List<Zone> actualList = zoneService.getZones();
        List<Zone> expectedList = new ArrayList<>();
        expectedList.add(new Zone(1, Arrays.asList("A","B")));
        expectedList.add(new Zone(2, Arrays.asList("C","D","E")));
        expectedList.add(new Zone(3, Arrays.asList("C","E","F")));
        expectedList.add(new Zone(4, Arrays.asList("F","G","H","I")));
        assertEquals(actualList,expectedList);
    }

    @Test
    public void getZoneByStationError() {
        List<Zone> actualList = zoneService.getZoneByStation("C");
        List<Zone> expectedList = new ArrayList<>();
        expectedList.add(new Zone(2, Arrays.asList("C","D","E")));
        expectedList.add(new Zone(3, Arrays.asList("C","E","I")));
        assertNotEquals(actualList,expectedList);

    }

    @Test
    public void getZoneByStation() {
        List<Zone> actualList = zoneService.getZoneByStation("C");
        List<Zone> expectedList = new ArrayList<>();
        expectedList.add(new Zone(2, Arrays.asList("C","D","E")));
        expectedList.add(new Zone(3, Arrays.asList("C","E","F")));
        assertEquals(actualList,expectedList);
    }
}