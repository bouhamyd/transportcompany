package com.company.train.services;

import com.company.train.models.input.Tap;
import com.company.train.models.input.Taps;
import com.company.train.models.output.CustomerSummarie;
import com.company.train.models.output.CustomerSummaries;
import com.company.train.models.output.Trip;
import com.company.train.util.TransportException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CutomerSummarieServiceTest {

    CutomerSummarieService cutomerSummarieService = new CutomerSummarieService();

    @Test
    public void getCustomerSummaries() throws JsonProcessingException {
        List<Tap> taps = new ArrayList<Tap>();
        taps.add(new Tap(1572242400,1,"A"));
        taps.add(new Tap(1572244200,1,"D"));
        taps.add(new Tap(1572282000,1,"D"));
        taps.add(new Tap(1572283800,1,"A"));
        CustomerSummaries actualCustomerSummarie = cutomerSummarieService.getCustomerSummaries(taps);
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(actualCustomerSummarie);
        System.out.println(jsonString);
        CustomerSummarie expectedCustomerSummarie = new CustomerSummarie();
        Trip expectedTrip= new Trip();
        expectedTrip.setZoneFrom(1);
        expectedTrip.setZoneTo(2);
        expectedTrip.setStationStart("A");
        expectedTrip.setStationEnd("D");
        expectedTrip.setCostInCents(240);
        expectedTrip.setStartedJourneyAt(1572242400);
        expectedCustomerSummarie.setTrips(Arrays.asList(expectedTrip));
        expectedCustomerSummarie.setCustomerId(1);
        expectedCustomerSummarie.setTotalCostInCents(480);
        assertEquals(String.valueOf(expectedCustomerSummarie.getCustomerId()),String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getCustomerId()));
        assertEquals(String.valueOf(expectedCustomerSummarie.getTotalCostInCents()),String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getTotalCostInCents()));
        assertEquals(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getStationStart(),expectedTrip.getStationStart());
        assertEquals(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getStationEnd(),expectedTrip.getStationEnd());
        assertEquals(String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getCostInCents()),String.valueOf(expectedTrip.getCostInCents()));
        assertEquals(String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getZoneFrom()),String.valueOf(expectedTrip.getZoneFrom()));
        assertEquals(String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getZoneTo()),String.valueOf(expectedTrip.getZoneTo()));
        assertEquals(String.valueOf(actualCustomerSummarie.getCustomerSummaries().get(0).getTrips().get(0).getStartedJourneyAt()),String.valueOf(expectedTrip.getStartedJourneyAt()));
    }


    @Test
    public void getCustomerSummaire() {
        List<Tap> taps = new ArrayList<Tap>();
        taps.add(new Tap(1572242400,1,"A"));
        taps.add(new Tap(1572244200,1,"D"));
        taps.add(new Tap(1572282000,1,"D"));
        taps.add(new Tap(1572283800,1,"A"));
        CustomerSummarie actualCustomerSummarie = cutomerSummarieService.getCustomerSummaire(taps);
        CustomerSummarie expectedCustomerSummarie = new CustomerSummarie();
        Trip expectedTrip= new Trip();
        expectedTrip.setZoneFrom(1);
        expectedTrip.setZoneTo(2);
        expectedTrip.setStationStart("A");
        expectedTrip.setStationEnd("D");
        expectedTrip.setCostInCents(240);
        expectedTrip.setStartedJourneyAt(1572242400);
        expectedCustomerSummarie.setTrips(Arrays.asList(expectedTrip));
        expectedCustomerSummarie.setCustomerId(1);
        expectedCustomerSummarie.setTotalCostInCents(480);
        assertEquals(String.valueOf(expectedCustomerSummarie.getCustomerId()),String.valueOf(actualCustomerSummarie.getCustomerId()));
        assertEquals(String.valueOf(expectedCustomerSummarie.getTotalCostInCents()),String.valueOf(actualCustomerSummarie.getTotalCostInCents()));
        assertEquals(actualCustomerSummarie.getTrips().get(0).getStationStart(),expectedTrip.getStationStart());
        assertEquals(actualCustomerSummarie.getTrips().get(0).getStationEnd(),expectedTrip.getStationEnd());
        assertEquals(String.valueOf(actualCustomerSummarie.getTrips().get(0).getCostInCents()),String.valueOf(expectedTrip.getCostInCents()));
        assertEquals(String.valueOf(actualCustomerSummarie.getTrips().get(0).getZoneFrom()),String.valueOf(expectedTrip.getZoneFrom()));
        assertEquals(String.valueOf(actualCustomerSummarie.getTrips().get(0).getZoneTo()),String.valueOf(expectedTrip.getZoneTo()));
        assertEquals(String.valueOf(actualCustomerSummarie.getTrips().get(0).getStartedJourneyAt()),String.valueOf(expectedTrip.getStartedJourneyAt()));
    }

    @Test
    public void customerSummariesToJson_Should_Return_Exception() {
        try {
            cutomerSummarieService.customerSummariesToJson(null,("c:/test/notFound/notfound.txt"));
            fail("should throw TransportException");
        }catch (TransportException e){
            assert(e.getMessage().contains("File not found"));
        }
    }
}