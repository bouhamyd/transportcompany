**Build manually the application:**

1- Clone the projet

git clone https://bitbucket.org/bouhamyd/transportcompany.git

2- Move into the directory projet

cd transportCompany

3- Execute UTs and build the application :

mvn package

4- Test it

java -jar target/transportCompany-0.0.1-SNAPSHOT.jar CandidateInputExample.txt CandidateOutputExample.txt

**Build with Jenkins:**
See the jenkinsfile
![Jenkins-pipeline](jenkins.PNG)